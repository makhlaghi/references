References
==========

This repository contains a classified set of references that I have found interesting during my work and daily reads of arXiv.

Links to papers
---------------

To link to an item/paper within this `references.org`:
- Go over the item and press `C-c l`. This will store the link in Emacs's memory.
- Go to any other org-mode file that you want to insert the link in. Then press `C-c C-l`, this will list all the previously stored links that you can select (with the up/down keys); press ENTER.
- Set a name for the link (if you want).

If these key-bindings do not work, add the following two lines to your `~/.emacs` file (as in [this example](https://codeberg.org/mohammad.akhlaghi/operating-system-customization/src/branch/master/config/emacs.conf))

```
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c C-l") 'org-insert-link)
```
